'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">web-awp-app documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AddressBookModule.html" data-type="entity-link" >AddressBookModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AddressBookModule-ca5a060e682a6bba17ae7bbde7ed1dd9"' : 'data-target="#xs-components-links-module-AddressBookModule-ca5a060e682a6bba17ae7bbde7ed1dd9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AddressBookModule-ca5a060e682a6bba17ae7bbde7ed1dd9"' :
                                            'id="xs-components-links-module-AddressBookModule-ca5a060e682a6bba17ae7bbde7ed1dd9"' }>
                                            <li class="link">
                                                <a href="components/AddressBookModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddressBookModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DialerControlsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DialerControlsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ExternalAddressComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ExternalAddressComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InternalAddressComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InternalAddressComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InternalBookWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InternalBookWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PhoneBookCallComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PhoneBookCallComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PhoneBookRecordComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PhoneBookRecordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PhoneBookWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PhoneBookWidgetComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-a7ac4267005325e46f2502e160866f40"' : 'data-target="#xs-components-links-module-AppModule-a7ac4267005325e46f2502e160866f40"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-a7ac4267005325e46f2502e160866f40"' :
                                            'id="xs-components-links-module-AppModule-a7ac4267005325e46f2502e160866f40"' }>
                                            <li class="link">
                                                <a href="components/ActivityControlWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ActivityControlWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddWidgetDialogComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddWidgetDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CallGroupSelectorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CallGroupSelectorComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CallRecordsModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CallRecordsModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChainIconComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChainIconComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DesignerCallControlWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DesignerCallControlWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DesktopRenameDialogComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DesktopRenameDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DtmfDialerComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DtmfDialerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditDesktopIconComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditDesktopIconComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FirstViewComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FirstViewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GroupDialogComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GroupDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HotkeyRowComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HotkeyRowComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HotkeySetModeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HotkeySetModeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HotkeysModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HotkeysModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/IframePluginWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IframePluginWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/IframeWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IframeWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InfoModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InfoModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InternalChatModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InternalChatModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/KeyboardBtnComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >KeyboardBtnComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LockReasonsModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LockReasonsModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MatBottomSheetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MatBottomSheetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MediaControllerComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MediaControllerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServiceCompleteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServiceCompleteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VerticalCallControlWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >VerticalCallControlWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WorkAreaBuilderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkAreaBuilderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WorkAreaChannelControlsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkAreaChannelControlsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WorkAreaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkAreaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WorkAreaDesktopsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkAreaDesktopsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WorkAreaDialogsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkAreaDialogsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WorkAreaGroupComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkAreaGroupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WorkAreaHeaderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkAreaHeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WorkAreaItemComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkAreaItemComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WorkAreaLockComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkAreaLockComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WorkplaceIdFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkplaceIdFormComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-AppModule-a7ac4267005325e46f2502e160866f40"' : 'data-target="#xs-pipes-links-module-AppModule-a7ac4267005325e46f2502e160866f40"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-AppModule-a7ac4267005325e46f2502e160866f40"' :
                                            'id="xs-pipes-links-module-AppModule-a7ac4267005325e46f2502e160866f40"' }>
                                            <li class="link">
                                                <a href="pipes/SafePipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SafePipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/UrlMacrosPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UrlMacrosPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CallsHistoryModule.html" data-type="entity-link" >CallsHistoryModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CallsHistoryModule-3f3b68a23c63c11e1d093bedb93a7c80"' : 'data-target="#xs-components-links-module-CallsHistoryModule-3f3b68a23c63c11e1d093bedb93a7c80"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CallsHistoryModule-3f3b68a23c63c11e1d093bedb93a7c80"' :
                                            'id="xs-components-links-module-CallsHistoryModule-3f3b68a23c63c11e1d093bedb93a7c80"' }>
                                            <li class="link">
                                                <a href="components/CallActionsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CallActionsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CallHistoryComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CallHistoryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CallRecordsCellRendererComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CallRecordsCellRendererComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CallsDashboardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CallsDashboardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DateRendererComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DateRendererComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PhoneNumberFilterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PhoneNumberFilterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PhoneNumberRendererComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PhoneNumberRendererComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TableFilterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TableFilterComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ChatModule.html" data-type="entity-link" >ChatModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ChatModule-deb654edd91f1419e88502cc69d80810"' : 'data-target="#xs-components-links-module-ChatModule-deb654edd91f1419e88502cc69d80810"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ChatModule-deb654edd91f1419e88502cc69d80810"' :
                                            'id="xs-components-links-module-ChatModule-deb654edd91f1419e88502cc69d80810"' }>
                                            <li class="link">
                                                <a href="components/ChatRoomComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChatRoomComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChatRoomTriggerComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChatRoomTriggerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChatTextInputComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChatTextInputComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChatWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChatWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CommonChatComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CommonChatComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GQLChatMessageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GQLChatMessageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InternalChatComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InternalChatComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InternalChatIconComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InternalChatIconComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VideoWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >VideoWidgetComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ChiefModule.html" data-type="entity-link" >ChiefModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ChiefModule-203dfec0f49f402a6c9f3c80bb00cd14"' : 'data-target="#xs-components-links-module-ChiefModule-203dfec0f49f402a6c9f3c80bb00cd14"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ChiefModule-203dfec0f49f402a6c9f3c80bb00cd14"' :
                                            'id="xs-components-links-module-ChiefModule-203dfec0f49f402a6c9f3c80bb00cd14"' }>
                                            <li class="link">
                                                <a href="components/AgentControlsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AgentControlsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AgentExtendedInfoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AgentExtendedInfoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AgentTypeFilterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AgentTypeFilterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChiefAgentsWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefAgentsWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChiefCommonControlComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefCommonControlComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChiefDialogsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefDialogsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChiefGroupsWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefGroupsWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChiefQueuesWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefQueuesWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChiefRightsWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefRightsWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChiefRolesWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefRolesWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChiefServicesWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefServicesWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChiefWiretapWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefWiretapWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GroupControlsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GroupControlsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/QueueControlsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >QueueControlsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoleFilterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RoleFilterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoleSelectorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RoleSelectorComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SearchHighlightingComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SearchHighlightingComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServiceControlsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServiceControlsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StatesRendererComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StatesRendererComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WpSetupComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WpSetupComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ChiefModule-203dfec0f49f402a6c9f3c80bb00cd14"' : 'data-target="#xs-injectables-links-module-ChiefModule-203dfec0f49f402a6c9f3c80bb00cd14"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ChiefModule-203dfec0f49f402a6c9f3c80bb00cd14"' :
                                        'id="xs-injectables-links-module-ChiefModule-203dfec0f49f402a6c9f3c80bb00cd14"' }>
                                        <li class="link">
                                            <a href="injectables/ChiefService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/WiretapService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WiretapService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ChiefStatisticsModule.html" data-type="entity-link" >ChiefStatisticsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ChiefStatisticsModule-c1466bb370a184bd157e7595a73a950f"' : 'data-target="#xs-components-links-module-ChiefStatisticsModule-c1466bb370a184bd157e7595a73a950f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ChiefStatisticsModule-c1466bb370a184bd157e7595a73a950f"' :
                                            'id="xs-components-links-module-ChiefStatisticsModule-c1466bb370a184bd157e7595a73a950f"' }>
                                            <li class="link">
                                                <a href="components/ChiefViewComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChiefViewComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/GraphQLModule.html" data-type="entity-link" >GraphQLModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/GroupListModule.html" data-type="entity-link" >GroupListModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-GroupListModule-e50f748f025eca26b7dfc614b09958eb"' : 'data-target="#xs-components-links-module-GroupListModule-e50f748f025eca26b7dfc614b09958eb"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-GroupListModule-e50f748f025eca26b7dfc614b09958eb"' :
                                            'id="xs-components-links-module-GroupListModule-e50f748f025eca26b7dfc614b09958eb"' }>
                                            <li class="link">
                                                <a href="components/GroupEntryComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GroupEntryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GroupListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GroupListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PersonalStatisticsModule.html" data-type="entity-link" >PersonalStatisticsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PersonalStatisticsModule-9d1d59ab3868e59ebf02d344f0a45569"' : 'data-target="#xs-components-links-module-PersonalStatisticsModule-9d1d59ab3868e59ebf02d344f0a45569"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PersonalStatisticsModule-9d1d59ab3868e59ebf02d344f0a45569"' :
                                            'id="xs-components-links-module-PersonalStatisticsModule-9d1d59ab3868e59ebf02d344f0a45569"' }>
                                            <li class="link">
                                                <a href="components/AgentViewComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AgentViewComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' : 'data-target="#xs-components-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' :
                                            'id="xs-components-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' }>
                                            <li class="link">
                                                <a href="components/ChipComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChipComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CopyNotificationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CopyNotificationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DialogComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HidingPanelComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HidingPanelComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HoverMenuComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HoverMenuComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InlineMessageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InlineMessageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProcessingTimeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProcessingTimeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoomsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RoomsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TableFilterHighlightingComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TableFilterHighlightingComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TransferChatIconComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TransferChatIconComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' : 'data-target="#xs-directives-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' :
                                        'id="xs-directives-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' }>
                                        <li class="link">
                                            <a href="directives/FocusDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FocusDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' : 'data-target="#xs-pipes-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' :
                                            'id="xs-pipes-links-module-SharedModule-884199db10058e110eaf3dc5cd9e35d5"' }>
                                            <li class="link">
                                                <a href="pipes/MatchHighlightingPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MatchHighlightingPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SpamListModule.html" data-type="entity-link" >SpamListModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SpamListModule-23eeab4975049bf587e23b1676158aff"' : 'data-target="#xs-components-links-module-SpamListModule-23eeab4975049bf587e23b1676158aff"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SpamListModule-23eeab4975049bf587e23b1676158aff"' :
                                            'id="xs-components-links-module-SpamListModule-23eeab4975049bf587e23b1676158aff"' }>
                                            <li class="link">
                                                <a href="components/DeleteControlComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DeleteControlComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SpamControlWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SpamControlWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SpamListWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SpamListWidgetComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SpamPhoneFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SpamPhoneFormComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-SpamListModule-23eeab4975049bf587e23b1676158aff"' : 'data-target="#xs-injectables-links-module-SpamListModule-23eeab4975049bf587e23b1676158aff"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-SpamListModule-23eeab4975049bf587e23b1676158aff"' :
                                        'id="xs-injectables-links-module-SpamListModule-23eeab4975049bf587e23b1676158aff"' }>
                                        <li class="link">
                                            <a href="injectables/SpamListService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SpamListService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/StatisticsModule.html" data-type="entity-link" >StatisticsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-StatisticsModule-0a4e1895d31c2ce1e12ac9b3256c4507"' : 'data-target="#xs-components-links-module-StatisticsModule-0a4e1895d31c2ce1e12ac9b3256c4507"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-StatisticsModule-0a4e1895d31c2ce1e12ac9b3256c4507"' :
                                            'id="xs-components-links-module-StatisticsModule-0a4e1895d31c2ce1e12ac9b3256c4507"' }>
                                            <li class="link">
                                                <a href="components/DestinationSelectComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DestinationSelectComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PeriodSelectComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PeriodSelectComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StatisticsViewComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StatisticsViewComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-StatisticsModule-0a4e1895d31c2ce1e12ac9b3256c4507"' : 'data-target="#xs-injectables-links-module-StatisticsModule-0a4e1895d31c2ce1e12ac9b3256c4507"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-StatisticsModule-0a4e1895d31c2ce1e12ac9b3256c4507"' :
                                        'id="xs-injectables-links-module-StatisticsModule-0a4e1895d31c2ce1e12ac9b3256c4507"' }>
                                        <li class="link">
                                            <a href="injectables/ConfigService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConfigService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/DataService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DataService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/SurveyWidgetModule.html" data-type="entity-link" >SurveyWidgetModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SurveyWidgetModule-ef9ac5e8cf09a5ccb35ee964d7dacbe0"' : 'data-target="#xs-components-links-module-SurveyWidgetModule-ef9ac5e8cf09a5ccb35ee964d7dacbe0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SurveyWidgetModule-ef9ac5e8cf09a5ccb35ee964d7dacbe0"' :
                                            'id="xs-components-links-module-SurveyWidgetModule-ef9ac5e8cf09a5ccb35ee964d7dacbe0"' }>
                                            <li class="link">
                                                <a href="components/SurveyWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SurveyWidgetComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TableModule.html" data-type="entity-link" >TableModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TableModule-22dc390b176234e5abb7c12d4292e595"' : 'data-target="#xs-components-links-module-TableModule-22dc390b176234e5abb7c12d4292e595"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TableModule-22dc390b176234e5abb7c12d4292e595"' :
                                            'id="xs-components-links-module-TableModule-22dc390b176234e5abb7c12d4292e595"' }>
                                            <li class="link">
                                                <a href="components/CheckboxFilterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CheckboxFilterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CheckboxRendererComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CheckboxRendererComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FilterByParsedFieldsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FilterByParsedFieldsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TableColsSelectorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TableColsSelectorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TMModule.html" data-type="entity-link" >TMModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TMModule-2bd1f1d1f83ab0f90a57e594dbeeb5c3"' : 'data-target="#xs-components-links-module-TMModule-2bd1f1d1f83ab0f90a57e594dbeeb5c3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TMModule-2bd1f1d1f83ab0f90a57e594dbeeb5c3"' :
                                            'id="xs-components-links-module-TMModule-2bd1f1d1f83ab0f90a57e594dbeeb5c3"' }>
                                            <li class="link">
                                                <a href="components/WorkplaceWidgetComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WorkplaceWidgetComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AuthorizationServiceEvent.html" data-type="entity-link" >AuthorizationServiceEvent</a>
                            </li>
                            <li class="link">
                                <a href="classes/AWPDesktopData.html" data-type="entity-link" >AWPDesktopData</a>
                            </li>
                            <li class="link">
                                <a href="classes/AWPGroupData.html" data-type="entity-link" >AWPGroupData</a>
                            </li>
                            <li class="link">
                                <a href="classes/CallRecord.html" data-type="entity-link" >CallRecord</a>
                            </li>
                            <li class="link">
                                <a href="classes/ChatCore.html" data-type="entity-link" >ChatCore</a>
                            </li>
                            <li class="link">
                                <a href="classes/CTAAgentWrapper.html" data-type="entity-link" >CTAAgentWrapper</a>
                            </li>
                            <li class="link">
                                <a href="classes/CTAElementContainer.html" data-type="entity-link" >CTAElementContainer</a>
                            </li>
                            <li class="link">
                                <a href="classes/CTAFullConfig.html" data-type="entity-link" >CTAFullConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/CTAPeriodTypes.html" data-type="entity-link" >CTAPeriodTypes</a>
                            </li>
                            <li class="link">
                                <a href="classes/CTAViewConfigurationContainer.html" data-type="entity-link" >CTAViewConfigurationContainer</a>
                            </li>
                            <li class="link">
                                <a href="classes/CustomMatErrorMatcher.html" data-type="entity-link" >CustomMatErrorMatcher</a>
                            </li>
                            <li class="link">
                                <a href="classes/Group.html" data-type="entity-link" >Group</a>
                            </li>
                            <li class="link">
                                <a href="classes/HTMLMenuSwitcher.html" data-type="entity-link" >HTMLMenuSwitcher</a>
                            </li>
                            <li class="link">
                                <a href="classes/ModelConverterContainer.html" data-type="entity-link" >ModelConverterContainer</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ActionService.html" data-type="entity-link" >ActionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ActivityControlService.html" data-type="entity-link" >ActivityControlService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ActivityService.html" data-type="entity-link" >ActivityService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AddMessageGQL.html" data-type="entity-link" >AddMessageGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AgentExtendedInfoService.html" data-type="entity-link" >AgentExtendedInfoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthorizationService.html" data-type="entity-link" >AuthorizationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AwpVersionService.html" data-type="entity-link" >AwpVersionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CallRecordsService.html" data-type="entity-link" >CallRecordsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CallService.html" data-type="entity-link" >CallService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ChatService.html" data-type="entity-link" >ChatService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ChiefMockService.html" data-type="entity-link" >ChiefMockService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ChiefService.html" data-type="entity-link" >ChiefService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CommonChatStoreService.html" data-type="entity-link" >CommonChatStoreService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConfigService.html" data-type="entity-link" >ConfigService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConfigurationService.html" data-type="entity-link" >ConfigurationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConfirmMessagesGQL.html" data-type="entity-link" >ConfirmMessagesGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CreateChatGQL.html" data-type="entity-link" >CreateChatGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CustomMatPaginatorIntl.html" data-type="entity-link" >CustomMatPaginatorIntl</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DataService.html" data-type="entity-link" >DataService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DeleteMessageGQL.html" data-type="entity-link" >DeleteMessageGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FinishCommonChatGQL.html" data-type="entity-link" >FinishCommonChatGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GetCallRecordLinkGQL.html" data-type="entity-link" >GetCallRecordLinkGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GetCallRecordsItemsGQL.html" data-type="entity-link" >GetCallRecordsItemsGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GetChatGQL.html" data-type="entity-link" >GetChatGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GetCommonChatsGQL.html" data-type="entity-link" >GetCommonChatsGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GetInternalChatsGQL.html" data-type="entity-link" >GetInternalChatsGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GetMessagesGQL.html" data-type="entity-link" >GetMessagesGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GlobalErrorHandler.html" data-type="entity-link" >GlobalErrorHandler</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/HotkeysService.html" data-type="entity-link" >HotkeysService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MediaService.html" data-type="entity-link" >MediaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificationService.html" data-type="entity-link" >NotificationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PersonalConfigService.html" data-type="entity-link" >PersonalConfigService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RolesService.html" data-type="entity-link" >RolesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SpamListService.html" data-type="entity-link" >SpamListService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StartMessageSubscriptionGQL.html" data-type="entity-link" >StartMessageSubscriptionGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StatisticsService.html" data-type="entity-link" >StatisticsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StopMessageSubscriptionGQL.html" data-type="entity-link" >StopMessageSubscriptionGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UIdService.html" data-type="entity-link" >UIdService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UpdateMessageGQL.html" data-type="entity-link" >UpdateMessageGQL</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WiretapMockService.html" data-type="entity-link" >WiretapMockService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WiretapService.html" data-type="entity-link" >WiretapService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WorkAreaBuilderService.html" data-type="entity-link" >WorkAreaBuilderService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WorkAreaColsService.html" data-type="entity-link" >WorkAreaColsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WorkAreaControlsService.html" data-type="entity-link" >WorkAreaControlsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WorkAreaDesktopsService.html" data-type="entity-link" >WorkAreaDesktopsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WorkAreaDialogsService.html" data-type="entity-link" >WorkAreaDialogsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WorkAreaGroupsService.html" data-type="entity-link" >WorkAreaGroupsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WorkAreaMenuService.html" data-type="entity-link" >WorkAreaMenuService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WorkAreaResizeService.html" data-type="entity-link" >WorkAreaResizeService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WorkAreaStorageService.html" data-type="entity-link" >WorkAreaStorageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WorkAreaWidgetsService.html" data-type="entity-link" >WorkAreaWidgetsService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/ActivitiesAction.html" data-type="entity-link" >ActivitiesAction</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Activity.html" data-type="entity-link" >Activity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ActivityActionEvent.html" data-type="entity-link" >ActivityActionEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ActivityControlChatEvent.html" data-type="entity-link" >ActivityControlChatEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ActivityControlServiceActions.html" data-type="entity-link" >ActivityControlServiceActions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ActivityControlSupport.html" data-type="entity-link" >ActivityControlSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AddNewMsgOpts.html" data-type="entity-link" >AddNewMsgOpts</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Agent2GroupAssociations.html" data-type="entity-link" >Agent2GroupAssociations</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AgentConfigurationResp.html" data-type="entity-link" >AgentConfigurationResp</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AgentFetchResponse.html" data-type="entity-link" >AgentFetchResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AgentSearchOptions.html" data-type="entity-link" >AgentSearchOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AgentTempTabUpdateData.html" data-type="entity-link" >AgentTempTabUpdateData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AgGridApiSupport.html" data-type="entity-link" >AgGridApiSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AudioFileState.html" data-type="entity-link" >AudioFileState</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPConfiguration.html" data-type="entity-link" >AWPConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPDesktop.html" data-type="entity-link" >AWPDesktop</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPGroupEvent.html" data-type="entity-link" >AWPGroupEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPGroupProfile.html" data-type="entity-link" >AWPGroupProfile</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPGroupsByType.html" data-type="entity-link" >AWPGroupsByType</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPMenuItem.html" data-type="entity-link" >AWPMenuItem</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPProfile.html" data-type="entity-link" >AWPProfile</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPUI.html" data-type="entity-link" >AWPUI</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPWidget.html" data-type="entity-link" >AWPWidget</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPWidgetCounts.html" data-type="entity-link" >AWPWidgetCounts</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPWidgetData.html" data-type="entity-link" >AWPWidgetData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AWPWidgetProfile.html" data-type="entity-link" >AWPWidgetProfile</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BuildData.html" data-type="entity-link" >BuildData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CachedChatData.html" data-type="entity-link" >CachedChatData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CallCommonFeaturesSupport.html" data-type="entity-link" >CallCommonFeaturesSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CallHistory.html" data-type="entity-link" >CallHistory</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CallHistoryRequestOptions.html" data-type="entity-link" >CallHistoryRequestOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CallSGA.html" data-type="entity-link" >CallSGA</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChatMessagePart.html" data-type="entity-link" >ChatMessagePart</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChatReadinessEvent.html" data-type="entity-link" >ChatReadinessEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChatsLoadedActivitiesEvent.html" data-type="entity-link" >ChatsLoadedActivitiesEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefAgentsResponse.html" data-type="entity-link" >ChiefAgentsResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefControl.html" data-type="entity-link" >ChiefControl</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefControlModes.html" data-type="entity-link" >ChiefControlModes</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefControlsSavings.html" data-type="entity-link" >ChiefControlsSavings</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefGroupEvent.html" data-type="entity-link" >ChiefGroupEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefGroupsResponse.html" data-type="entity-link" >ChiefGroupsResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefQueuesResponse.html" data-type="entity-link" >ChiefQueuesResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefServicesResponse.html" data-type="entity-link" >ChiefServicesResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefTableAgent.html" data-type="entity-link" >ChiefTableAgent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefTableAgentSelected.html" data-type="entity-link" >ChiefTableAgentSelected</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefTableCallQueue.html" data-type="entity-link" >ChiefTableCallQueue</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefTableGroup.html" data-type="entity-link" >ChiefTableGroup</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChiefTableService.html" data-type="entity-link" >ChiefTableService</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CommonChatEvent.html" data-type="entity-link" >CommonChatEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CompleteService.html" data-type="entity-link" >CompleteService</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ContingentInfo.html" data-type="entity-link" >ContingentInfo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CopyTableCellSupport.html" data-type="entity-link" >CopyTableCellSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CSAEvent.html" data-type="entity-link" >CSAEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAAgent.html" data-type="entity-link" >CTAAgent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAConfig.html" data-type="entity-link" >CTAConfig</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTADashboardConfiguration.html" data-type="entity-link" >CTADashboardConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTADashboardElementConfiguration.html" data-type="entity-link" >CTADashboardElementConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTADashboardElementViewConfiguration.html" data-type="entity-link" >CTADashboardElementViewConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTADestinationConfiguration.html" data-type="entity-link" >CTADestinationConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAGroup.html" data-type="entity-link" >CTAGroup</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAGroup2Agent.html" data-type="entity-link" >CTAGroup2Agent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAId.html" data-type="entity-link" >CTAId</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAIdName.html" data-type="entity-link" >CTAIdName</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAPeriod.html" data-type="entity-link" >CTAPeriod</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAPeriodType.html" data-type="entity-link" >CTAPeriodType</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAProfile.html" data-type="entity-link" >CTAProfile</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTARegion.html" data-type="entity-link" >CTARegion</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAService.html" data-type="entity-link" >CTAService</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAService2Group.html" data-type="entity-link" >CTAService2Group</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAStatisticsConfiguration.html" data-type="entity-link" >CTAStatisticsConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAStatisticsCustomer.html" data-type="entity-link" >CTAStatisticsCustomer</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAStatisticsLoadedResults.html" data-type="entity-link" >CTAStatisticsLoadedResults</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAStatisticsParameter.html" data-type="entity-link" >CTAStatisticsParameter</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAStatisticsParametersConfiguration.html" data-type="entity-link" >CTAStatisticsParametersConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAStatisticsParametersGroup.html" data-type="entity-link" >CTAStatisticsParametersGroup</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAStatisticsResults.html" data-type="entity-link" >CTAStatisticsResults</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAViewConfiguration.html" data-type="entity-link" >CTAViewConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAViewParameter.html" data-type="entity-link" >CTAViewParameter</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CTAWord.html" data-type="entity-link" >CTAWord</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CurrentAgentData.html" data-type="entity-link" >CurrentAgentData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CustomMenuItem.html" data-type="entity-link" >CustomMenuItem</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CustomTableColumn.html" data-type="entity-link" >CustomTableColumn</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DelayedInvokeAccOptions.html" data-type="entity-link" >DelayedInvokeAccOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DeleteBtnParams.html" data-type="entity-link" >DeleteBtnParams</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DeleteBtnSupport.html" data-type="entity-link" >DeleteBtnSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DialerData.html" data-type="entity-link" >DialerData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DraftMsgLSData.html" data-type="entity-link" >DraftMsgLSData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DraftMsgStateChangedData.html" data-type="entity-link" >DraftMsgStateChangedData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/EntityLinks.html" data-type="entity-link" >EntityLinks</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ErrorHandlerSupport.html" data-type="entity-link" >ErrorHandlerSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ExtraData.html" data-type="entity-link" >ExtraData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ExtraDataLabels.html" data-type="entity-link" >ExtraDataLabels</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ExtraService.html" data-type="entity-link" >ExtraService</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GetChatsSubscriptionsData.html" data-type="entity-link" >GetChatsSubscriptionsData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GetMsgRefOptions.html" data-type="entity-link" >GetMsgRefOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GQLConfig.html" data-type="entity-link" >GQLConfig</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GQLGetMessagesOptions.html" data-type="entity-link" >GQLGetMessagesOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GQLRenderMessagesOptions.html" data-type="entity-link" >GQLRenderMessagesOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GraphQLChatSupport.html" data-type="entity-link" >GraphQLChatSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GroupMenuItem.html" data-type="entity-link" >GroupMenuItem</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GroupWithAssociatedServices.html" data-type="entity-link" >GroupWithAssociatedServices</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/HeaderDataEmitter.html" data-type="entity-link" >HeaderDataEmitter</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/HKActionsMap.html" data-type="entity-link" >HKActionsMap</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/HKCheckResult.html" data-type="entity-link" >HKCheckResult</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/HotkeyConfig.html" data-type="entity-link" >HotkeyConfig</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/HTMLChild.html" data-type="entity-link" >HTMLChild</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/HTMLSwitchFocusSupport.html" data-type="entity-link" >HTMLSwitchFocusSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/InternalChatVisibilitySubjectData.html" data-type="entity-link" >InternalChatVisibilitySubjectData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LanguageNames.html" data-type="entity-link" >LanguageNames</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LastScrollLoadingChatData.html" data-type="entity-link" >LastScrollLoadingChatData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ListItem.html" data-type="entity-link" >ListItem</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MessagesCountData.html" data-type="entity-link" >MessagesCountData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MsgAction.html" data-type="entity-link" >MsgAction</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MsgInputStateData.html" data-type="entity-link" >MsgInputStateData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/NewChatActivitiesEvent.html" data-type="entity-link" >NewChatActivitiesEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/NoResultsMessage.html" data-type="entity-link" >NoResultsMessage</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/NotificationMessage.html" data-type="entity-link" >NotificationMessage</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PendingMessagesData.html" data-type="entity-link" >PendingMessagesData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PhoneBookRecord.html" data-type="entity-link" >PhoneBookRecord</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PostponedMethodData.html" data-type="entity-link" >PostponedMethodData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PostponedMethodsMapData.html" data-type="entity-link" >PostponedMethodsMapData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PriorityFetchControl.html" data-type="entity-link" >PriorityFetchControl</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ProcessingTimesOpts.html" data-type="entity-link" >ProcessingTimesOpts</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RequestIndicationSupport.html" data-type="entity-link" >RequestIndicationSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ResizablePanelOptions.html" data-type="entity-link" >ResizablePanelOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RoleRule.html" data-type="entity-link" >RoleRule</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ScrollToCertainMsgOptions.html" data-type="entity-link" >ScrollToCertainMsgOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SearchFilterData.html" data-type="entity-link" >SearchFilterData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SelectGroupSupport.html" data-type="entity-link" >SelectGroupSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SelectSupport.html" data-type="entity-link" >SelectSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Service2GroupAssociations.html" data-type="entity-link" >Service2GroupAssociations</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ServiceWithAssociatedGroups.html" data-type="entity-link" >ServiceWithAssociatedGroups</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SimpleNotificationHandlerOpts.html" data-type="entity-link" >SimpleNotificationHandlerOpts</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SimplifiedMsgOptions.html" data-type="entity-link" >SimplifiedMsgOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SortedHK.html" data-type="entity-link" >SortedHK</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SpamConfigEntry.html" data-type="entity-link" >SpamConfigEntry</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SpamEntry.html" data-type="entity-link" >SpamEntry</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SpamListConfigResponse.html" data-type="entity-link" >SpamListConfigResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SpamListDataResponse.html" data-type="entity-link" >SpamListDataResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SpamListFindResponse.html" data-type="entity-link" >SpamListFindResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SpamListResponse.html" data-type="entity-link" >SpamListResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StatisticsFilterOpening.html" data-type="entity-link" >StatisticsFilterOpening</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StatisticsSearchData.html" data-type="entity-link" >StatisticsSearchData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StatisticsSupport.html" data-type="entity-link" >StatisticsSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SubjectAgent.html" data-type="entity-link" >SubjectAgent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TableCellClickEvent.html" data-type="entity-link" >TableCellClickEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TableFilterConfigs.html" data-type="entity-link" >TableFilterConfigs</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TableFilterSearchSupport.html" data-type="entity-link" >TableFilterSearchSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TableSaveSupport.html" data-type="entity-link" >TableSaveSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TempChatsCacheMapData.html" data-type="entity-link" >TempChatsCacheMapData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Timespan.html" data-type="entity-link" >Timespan</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TranslateSupport.html" data-type="entity-link" >TranslateSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserEntries.html" data-type="entity-link" >UserEntries</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/WidgetPosition.html" data-type="entity-link" >WidgetPosition</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/WiretapActionData.html" data-type="entity-link" >WiretapActionData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/WiretapActionSupport.html" data-type="entity-link" >WiretapActionSupport</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/WithDraftMsg.html" data-type="entity-link" >WithDraftMsg</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});